/**
 * @file segment.c
 * Функції роботи з сегментом
 * @author Поліна Федюкіна 
 * @version 1.0.0
 * @date 2020.05.26
 */

#include "../inc/segment.h"


double get_length(struct line_segment segment)
{
    return sqrt(pow((segment.B.x - segment.A.x), 2) + pow((segment.B.y - segment.A.y),2));
}

int is_point_on_line(struct point_cartesian point, struct line_segment segment)
{
    struct line_segment a = { segment.A , {point.x, point.y}},
                        b = {{point.x, point.y}, segment.B};

    return get_length(a) + get_length(b) == get_length(segment) ? 1 : 0;
}

void print_segment_points(struct line_segment segment)
{
    char out_area[SIZE_Y][SIZE_X];
    printf("\n");
    for(int i = 0; i < SIZE_Y; i++)
        for(int j = 0; j < SIZE_X; j++) {
            out_area[i][j] = ' ';
            if(j == MID_X) out_area[i][j] = '|';
            if(i == MID_Y) out_area[i][j] = '-';
            if(i == MID_Y && j == MID_X) out_area[i][j] = '+';
        }

    out_area[MID_Y - (int)segment.A.y][MID_X + (int)segment.A.x] = '*';
    out_area[MID_Y - (int)segment.B.y][MID_X + (int)segment.B.x] = '*';
    for(int i = 0; i < SIZE_Y; i++)
    {
        for(int j = 0; j < SIZE_X; j++)
            printf("%c", out_area[i][j]);
        printf("\n");
    }
}

void print_segment(struct line_segment segment)
{

    char out_area[SIZE_Y][SIZE_X];
    printf("\n");
    for(int i = 0; i < SIZE_Y; i++)
        for(int j = 0; j < SIZE_X; j++) {
            out_area[i][j] = ' ';
            if(j == MID_X) out_area[i][j] = '|';
            if(i == MID_Y) out_area[i][j] = '-';
            if(i == MID_Y  && j == MID_X) out_area[i][j] = '+';
        }

    int l = (int)get_length(segment);
    int lx = segment.B.x - segment.A.x;
    int ly = segment.B.y - segment.A.y;

    float stepx = (float)lx/(float)l;
    float stepy = (float)ly/(float)l;
    float x = segment.A.x, y = segment.A.y;
    out_area[MID_Y - (int)segment.A.y][MID_X + (int)segment.A.x] = '*';
    out_area[MID_Y - (int)segment.B.y][MID_X + (int)segment.B.x] = '*';
    for(int i = 1; i < l; i++)
    {
        y += stepy;
        x += stepx;
        out_area[MID_Y - (int)roundf(y + 0.5)][MID_X+ (int)roundf(x + 0.5)] = '*';
    }
    for(int i = 0; i < SIZE_Y; i++)
    {
        for(int j = 0; j < SIZE_X; j++)
            printf("%c", out_area[i][j]);
        printf("\n");
    }
}

void print_sin()
{

    char out_area[SIZE_Y][SIZE_X];
    printf("\n");
    for(int i = 0; i < SIZE_Y; i++)
        for(int j = 0; j < SIZE_X; j++) {
            out_area[i][j] = ' ';
            if(j == MID_X) out_area[i][j] = '|';
            if(i == MID_Y) out_area[i][j] = '-';
            if(i == MID_Y && j == MID_X) out_area[i][j] = '+';
        }

    float y = 0;
    int ampl = 5;
    for(float i = 0; roundf(i*ampl) + MID_X < SIZE_X; i += 0.1)
    {
        y = sin(i) * 20;
        out_area[(int)roundf(MID_Y - y)][(int)roundf(i*ampl) + MID_X] = '*';
    }
    for(float i = 0; MID_X - roundf(i*ampl) >= 0; i += 0.1)
    {
        y = sin(i) * (-20);
        out_area[(int)roundf(MID_Y - y)][MID_X - (int)roundf(i*ampl)] = '*';
    }
    for(int i = 0; i < SIZE_Y; i++)
    {
        for(int j = 0; j < SIZE_X; j++)
            printf("%c", out_area[i][j]);
        printf("\n");
    }
}


void print_diagram(const char * word, int diagram_size, char fill_symbol)
{
    int w_size = strlen(word);
    static int size = 256;
    char symbols[size];
    for(int i = 0; i < size; i++)
        symbols[i] = 0;

    int max = symbols[0];
    for(int i = 0; i < w_size; i++)
    {
        symbols[word[i]]++;
        if(symbols[word[i]] > max) max = symbols[word[i]];
    }
    int step = diagram_size / max;

    for(int i = 0; i < size; i++)
    {
        if(!symbols[i]) continue;
        printf(" %c => ", i);
        for(int j = 0; j < symbols[i] * step; j++)
            printf("%c", fill_symbol);
        printf("\n");
    }
}
