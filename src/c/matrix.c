/**
 * @file matrix.c
 * Функції роботи з матрицями
 * @author Поліна Федюкіна
 * @version 1.0.0
 * @date 2020.06.02
 */

#include "../inc/matrix.h"

void print_2d_array(int * array, int rows, int cols)
{
    for(int i = 0; i < rows * cols; i++)
    {
        if(!(i*rows % cols) && i) printf("\n");
        printf("%5i ", array[i]); 
    }
    printf("\n");
}

void print_matrix(struct matrix source)
{
    for(int i = 0; i < source.rows; i++)
    {
        for(int j = 0; j < source.cols; j++)    
            printf("%7.2f ", source.data[i*source.cols + j]);
        printf("\n");
    }
    printf("\n");
}

struct matrix copy_matrix(struct matrix source)
{
    struct matrix res;
    res.cols = source.cols;
    res.rows = source.rows;
    res.data = (float*)malloc(sizeof(float)* res.rows * res.cols);
    for(int i = 0; i < source.cols * source.rows; i++)
        res.data[i] = source.data[i];
    return res;
}

struct matrix mul_matrix_by_value(struct matrix source, double lamda)
{
    struct matrix temp = copy_matrix(source);
    for(int i = 0; i < source.rows * source.cols; i++)
        temp.data[i] *= lamda;
    return temp;
}

struct matrix sum_matrix(struct matrix source1, struct matrix source2)
{
    struct matrix temp = copy_matrix(source1);
    for(int i = 0; i < temp.rows * temp.cols; i++)
        temp.data[i] += source2.data[i];

    return temp;
}

struct matrix mul_matrix(struct matrix source1, struct matrix source2)
{
    struct matrix temp;
    temp.rows = source1.rows;
    temp.cols = source2.cols;

    temp.data = (float*)malloc(sizeof(float) * temp.rows * temp.cols);
    
    for(int i = 0; i < source1.rows; i++)
        for(int j = 0; j < source2.cols; j++)
        {
            temp.data[i*temp.rows + j] = 0;
            for(int r = 0; r < source1.cols; r++)
                temp.data[i*temp.rows + j] += source1.data[i * source1.cols + r] * source2.data[ r * source2.cols + j];
        }

    return temp;
}

struct matrix transpose_matrix(struct matrix source)
{
    struct matrix temp;
    temp.cols = source.rows;
    temp.rows = source.cols;
    temp.data = (float*)malloc(sizeof(float) * temp.rows * temp.cols);
    float** matrix1 = (float**)malloc(sizeof(float*) * source.rows);

    for(int i = 0, k = 0; i < source.rows; i++)
    {
        matrix1[i] = (float*)malloc(sizeof(float) * source.cols);
        for(int j = 0; j < source.cols; j++)
            matrix1[i][j] = source.data[k++];
    }
    printf("\n");
    float** matrix2 = (float**)malloc(sizeof(float*) * temp.rows);
    for(int i = 0, k = 0; i < temp.rows; i++)
    {
        matrix2[i] = (float*)malloc(sizeof(float) * temp.cols);
        for(int j = 0; j < temp.cols; j++)
            matrix2[i][j] = matrix1[j][i];
    }

    for(int i = 0, k = 0; i < temp.rows; i++)
        for(int j = 0; j < temp.cols; j++)
            temp.data[k++] = matrix2[i][j];

    for(int i = 0; i < source.rows; i++)
        free(matrix1[i]);

    for(int i = 0; i < source.cols; i++)
        free(matrix2[i]);

    free(matrix1);
    free(matrix2);
    return temp;
}

double get_matrix_determinant(struct matrix source)
{
    if(source.cols != source.rows) return 0;
    if(source.cols == 1) return source.data[0];

    double det = 0;
    float** m1 = (float**)malloc(sizeof(float*) * source.rows);
    for(int i = 0, k = 0; i < source.rows; i++)
    {
        m1[i] = (float*)malloc(sizeof(float) * source.cols);
        for(int j = 0; j < source.cols; j++)
            m1[i][j] = source.data[k++];
    }
    int size = source.cols;
    
    float* arr = (float*)malloc(sizeof(float) * (size - 1));

    struct matrix temp;
    temp.cols = size - 1;
    temp.rows = size - 1;

    for(int i = 0; i < size; i++)
    {
        for(int j = 1, p = 0; j < size; j++)
            for(int k = 0; k < size; k++)
            {
                if(k == i) continue;
                arr[p++] = m1[j][k];
            }
        temp.data = arr;
        det += m1[0][i] * pow(-1, i) * get_matrix_determinant(temp);
    }
    free(m1);
    free(arr);
    return det;
}

struct matrix get_inverse_matrix(struct matrix source)
{
    float** m1 = (float**)malloc(sizeof(float*) * source.rows);
    for(int i = 0, k = 0; i < source.rows; i++)
    {
        m1[i] = (float*)malloc(sizeof(float) * source.cols);
        for(int j = 0; j < source.cols; j++)
            m1[i][j] = source.data[k++];
    }

    int size = source.cols;
    struct matrix m, temp, m2;
    temp.cols = size - 1;
    temp.rows = temp.cols;
    float* arr = (float*)malloc(sizeof(float) * (size - 1));
    float* arr2 = (float*)malloc(sizeof(float) * size * size);
    int a = 1, b = 2;
    for(int a = 0, k = 0; a < size; a++)
    {
        for(int b = 0; b < size; b++)
        {
            for(int i = 0, p = 0; i < size; i++) 
            {
                if(i == a) continue;
                for(int j = 0; j < size; j++)
                {
                    if(j == b) continue;
                    arr[p++] = m1[i][j];
                }
                
            }
            temp.data = arr;
            arr2[k++] = get_matrix_determinant(temp) * pow(-1, a + b);
        }
    }
    temp.cols = size;
    temp.rows = size;
    temp.data = arr2;

    m = transpose_matrix(temp);
    m2 = mul_matrix_by_value(m, 1/fabs(get_matrix_determinant(source)));
    return m2;
}
