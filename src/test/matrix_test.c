/**
 * @file matrix_test.c
 * Тестовий файл для matrix.c
 * @author Поліна Федюкіна
 * @version 1.0.0
 * @date 2020.06.02
 */

#include <cgreen/cgreen.h>
#include "../inc/matrix.h"

Describe(Matrix);
BeforeEach(Matrix)
{
}
AfterEach(Matrix)
{
}

Ensure(Matrix, returns_matrix_copy)
{
    const int a = 2, b = 3;
	float input[a*b] = { 1, 0, 10, 0.17, 6.28, 4.89 };
    struct matrix test = {a, b, input}, result;
    result = copy_matrix(test);
    int status = 0;
    assert_that(&test.data[0], is_not_equal_to(&result.data[0]));
    for(int i = 0; i < a*b; i++)
		if(test.data[i] != result.data[i])
		{
			status = 1;
			break;
		}
	assert_that(status, is_not_equal_to(1));
}

Ensure(Matrix, returns_mul_matrix_by_value)
{
    const int a = 2, b = 3, tests = 2;
	float input[a*b] = { 1, 0, 10, 0.17, 6.28, 4.89 };
	float res[tests][a*b] = { {0, 0, 0, 0, 0, 0}, {5, 0, 50, 0.85, 31.40, 24.45}};
	double lambda[tests] = {0, 5};
    struct matrix test = {a, b, input}, result;
	significant_figures_for_assert_double_are(2);
	for(int j = 0; j < tests; j++)
	{
    	result = mul_matrix_by_value(test, lambda[j]);
		for(int i = 0; i < a*b; i++)
			assert_that_double(result.data[i], is_equal_to_double(res[j][i]));
	}
}


Ensure(Matrix, returns_matrix_sum)
{
    const int a = 2, b = 3;
	float input[a*b] = { 1, 0, 10, 0.17, 6.28, 4.89 };
	float res[a*b] = { 2, 0, 20, 0.34, 12.56, 9.78};
    struct matrix test = {a, b, input}, result;
    result = sum_matrix(test, test);

	significant_figures_for_assert_double_are(2);
	for(int j = 0; j < a*b; j++)
    	assert_that_double(res[j], is_equal_to_double(result.data[j]));
	
}

Ensure(Matrix, returns_matrix_mul)
{
    const int a = 2, b = 3;
	float input[a*b] = { 1,2,3,4,5,6};
	float res[b*b] = {9,12,15,19,26,33,29,40,51};
    struct matrix test1 = {b, a, input}, test2 = {a, b, input}, result;
    result = mul_matrix(test1, test2);

	significant_figures_for_assert_double_are(2);
	for(int j = 0; j < a*b; j++)
    	assert_that_double(res[j], is_equal_to_double(result.data[j]));
	
}

Ensure(Matrix, returns_transpose_matrix)
{
    const int a = 3, tests = 2;
	float input[a*a] = { 1,2,3,4,5,6, 7, 8, 9};
	float res[a*a] = { 1, 4 , 7, 2 ,5 ,8, 3 ,6, 9};
    struct matrix test1 = {a, a, input}, result;
    result = transpose_matrix(test1);

	significant_figures_for_assert_double_are(2);
	for(int j = 0; j < a*a; j++)
    	assert_that_double(res[j], is_equal_to_double(result.data[j]));
	
}

Ensure(Matrix, returns_matrix_determinant)
{
    const int a = 3, b = 2, tests = 2;
	float input[tests][a*a] = { {1, 2, 4, 5}, {1, 2, 3, 4, 5, 6, 7, 8, 9}};
	float res[tests] = { -3, 0};
    struct matrix test1 = {b, b, input[0]}, test2 = {a, a, input[1]}, result;
    assert_that_double(get_matrix_determinant(test1), is_equal_to_double(res[0]));
	assert_that_double(get_matrix_determinant(test2), is_equal_to_double(res[1]));
}

Ensure(Matrix, returns_inverse_matrix)
{
    const int a = 4, b = 2, tests = 2;
	float input[tests][a*a] = { {1, 2, 4, 5}, {7,8,9,1,1,2,3,4,4,5,6,7,7,9,8,2}};
	float res[tests][a*a] = { {1.66, -0.66, -1.33, 0.33}, 
							{0.1, -1.37, 0.85, -0.33, -0.48, 0.62, -0.48, 0.66, 0.4, 0.51, -0.25, -0.33, -0.11, -0.11, 0.22, 0}};
    struct matrix test1 = {b, b, input[0]}, test2 = {a, a, input[1]}, result;
	significant_figures_for_assert_double_are(2);
	result = get_inverse_matrix(test1);
	for(int i = 0; i < b*b; i++)
		assert_that_double(res[0][i], is_equal_to_double(result.data[i]));
	result = get_inverse_matrix(test2);
	for(int i = 0; i < a*a; i++)
		assert_that_double(res[1][i], is_equal_to_double(result.data[i]));
	
}

TestSuite *matrix_test()
{
	TestSuite *suite = create_test_suite();
	add_test_with_context(suite, Matrix, returns_matrix_copy);
	add_test_with_context(suite, Matrix, returns_mul_matrix_by_value);
	add_test_with_context(suite, Matrix, returns_matrix_sum);
	add_test_with_context(suite, Matrix, returns_matrix_mul);
	add_test_with_context(suite, Matrix, returns_transpose_matrix);
	add_test_with_context(suite, Matrix, returns_matrix_determinant);
	add_test_with_context(suite, Matrix, returns_inverse_matrix);
	return suite;
}
