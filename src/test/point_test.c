/**
 * @file point_tests.c
 * Тестування функцій з point.c
 * @author Поліна Федюкіна
 * @version 1.0.0
 * @date 2020.03.03
 */

#include <cgreen/cgreen.h>

#include "../inc/point.h"

Describe(Point);
BeforeEach(Point)
{
}
AfterEach(Point)
{
}

Ensure(Point, returns_cartesian)
{
	const int tests = 5;
	double input[tests][2] = { { 0.0f, 0.0f },
				   { 25, 0.0f },
				   { 25, 180 },
				   { 25, -90 },
				   { 14.14, 45 } };
	double result[tests][2] = { { 0.00, 0.00 },
				    { 25, 0.00f },
				    { -25, 0.00 },
				    { 0.0f, -25 },
				    { 10, 10 } };
	significant_figures_for_assert_double_are(0);
	struct point_polar point;
	for (unsigned i = 0; i < tests; i++) {
		point.r = input[i][0];
		point.a = input[i][1];
		assert_that_double(polar_to_cartesian(point).x,
				   is_equal_to_double(result[i][0]));
		assert_that_double(polar_to_cartesian(point).y,
				   is_equal_to_double(result[i][1]));
	}
}

Ensure(Point, returns_polar)
{
	const int tests = 5;
	double result[tests][2] = { { -25, -25 },
				    { 0.0f, 0.0f },
				    { 0.0f, 25 },
				    { 10, -10 },
				    { -10, 10 } };
	double input[tests][2] = { { 35.36, -135 },
				   { 0.0f, 0.0f },
				   { 25, 90 },
				   { 14.14, -45 },
				   { 14.14, 135 } };
	significant_figures_for_assert_double_are(0);
	struct point_cartesian point;
	for (unsigned i = 0; i < tests; i++) {
		point.x = input[i][0];
		point.y = input[i][1];
		assert_that_double(cartesian_to_polar(point).r,
				   is_equal_to_double(result[i][0]));
		assert_that_double(cartesian_to_polar(point).a,
				   is_equal_to_double(result[i][1]));
	}
}

TestSuite *point_test()
{
	TestSuite *suite = create_test_suite();
	add_test_with_context(suite, Point, returns_cartesian);
	add_test_with_context(suite, Point, returns_polar);
	return suite;
}
