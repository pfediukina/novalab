/**
 * @file angles.h
 * Заголовний файл для angles.c. Містить прототипи функцій та необхідні бібліотеки
 * @author Поліна Федюкіна
 * @version 1.0.0
 * @date 2020.03.03
 */

#include <math.h>


/**
 * Конвертує радіани до градусів
 * @param radians - кут у радіанах
 * @return кут у градусах
 */
double radians_to_degrees(double radians);

/**
 * Конвертує градуси до радіанів
 * @param degrees - кут у градусах
 * @return кут у радіанах
 */
double degrees_to_radians(double degrees);
