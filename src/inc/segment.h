/**
 * @file segment.h
 * Заголовний файл для parser.c. Містить прототипи функцій та необхідні бібліотеки.
 * @author Поліна Федюкіна
 * @version 1.0.0
 * @date 2020.05.19
 */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "point.h"

#define SIZE_X 140
#define SIZE_Y 45
#define MID_X SIZE_X/2 - 1
#define MID_Y SIZE_Y - 1

/**
 * @struct line_segment
 * @brief Описує відрізок
 * @var A - позиція першої точки, описана за допомогою point_cartesian
 * @var B - позиція другої точки, описана за допомогою point_cartesian
 */
struct line_segment
{
    struct point_cartesian A;
    struct point_cartesian B;
};

/**
 * Повертає довжину відрізка з сегменту line_segment
 * @param segment - сегмент
 * @return довжину
 */
double get_length(struct line_segment);

/**
 * Повертає принадлежність точки до відрізку
 * @param point - точка, описана структурою point_cartesian
 * @param segment - відрізок, описаний структурою line_segment
 * @return 1 - якщо точка лежить на відрізку; 0 - якщо точка не належить відрізку
 */
int is_point_on_line(struct point_cartesian point, struct line_segment segment);

/**
 * Виводить у декартовій системі точки відрізку
 * @param segment - сегмент, точки якого будуть відображені в системі
 */
void print_segment_points(struct line_segment segment);

/**
 * Виводить у декартовій системі відрізок
 * @param segment - відрізок
 */
void print_segment(struct line_segment segment);

/**
 * Виводить у декартовій системі графік синуса
 */
void print_sin();

/**
 * Виводить діаграму символів у слові відносно довжини
 * @param word - слово
 * @param diagram_size - розмір діаграми
 * @param fill_symbol - символ для стовпців діаграми
 */
void print_diagram(const char * word, int diagram_size, char fill_symbol);
